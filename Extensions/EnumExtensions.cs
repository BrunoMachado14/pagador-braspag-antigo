﻿namespace PaymentCwi.Extensions
{
	using System;

	/// <summary>
	/// The enum extensions.
	/// </summary>
	public static class EnumExtensions
	{
		/// <summary>The get description of Enum.</summary>
		/// <param name="EnumValue">The enum value.</param>
		/// <typeparam name="TEnum"></typeparam>
		/// <returns>The description of enum's value.</returns>
		public static string GetDescription<TEnum>(this TEnum EnumValue) where TEnum : struct
		{
			return Enum.GetName(typeof(TEnum), EnumValue);
		}
	}
}