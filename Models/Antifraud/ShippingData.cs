﻿namespace PaymentCwi.Models.Antifraud
{
    using PaymentCwi.Models.Enum;

    /// <summary>
    /// The shipping data.
    /// </summary>
    public class ShippingData
    {
        /// <summary>
        /// Gets or sets the addressee.
        /// </summary>
        public string Addressee { get; set; }

        /// <summary>
        /// Gets or sets the phone.
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Gets or sets the method.
        /// </summary>
        public ShippingMethodEnum Method { get; set; }

	    /// <summary>
	    /// Gets or sets the email.
	    /// </summary>
	    public string Email { get; set; }
	}
}