﻿namespace PaymentCwi.Models.Antifraud
{
    using PaymentCwi.Models.Enum;

    /// <summary>
    /// The item data.
    /// </summary>
    public class ItemData
    {
        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        public ProductTypeEnum Type { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the risk.
        /// </summary>
        public HedgeEnum Risk { get; set; }

        /// <summary>
        /// Gets or sets the sku.
        /// </summary>
        public string Sku { get; set; }

        /// <summary>
        /// Gets or sets the unit price.
        /// </summary>
        public long UnitPrice { get; set; }

        /// <summary>
        /// Gets or sets the quantity.
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// Gets or sets the host hedge.
        /// </summary>
        public HedgeEnum HostHedge { get; set; }

        /// <summary>
        /// Gets or sets the non sensical hedge.
        /// </summary>
        public HedgeEnum NonSensicalHedge { get; set; }

        /// <summary>
        /// Gets or sets the obscenities hedge.
        /// </summary>
        public HedgeEnum ObscenitiesHedge { get; set; }

        /// <summary>
        /// Gets or sets the phone hedge.
        /// </summary>
        public HedgeEnum PhoneHedge { get; set; }

        /// <summary>
        /// Gets or sets the time hedge.
        /// </summary>
        public HedgeEnum TimeHedge { get; set; }

        /// <summary>
        /// Gets or sets the velocity hedge.
        /// </summary>
        public HedgeEnum VelocityHedge { get; set; }

        /// <summary>
        /// Gets or sets the gift category.
        /// </summary>
        public GiftCategoryEnum GiftCategory { get; set; }

        /// <summary>
        /// Gets or sets the passenger.
        /// </summary>
        public PassengerData Passenger { get; set; }
    }
}