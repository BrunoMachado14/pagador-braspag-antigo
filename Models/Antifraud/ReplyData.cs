﻿namespace PaymentCwi.Models.Antifraud
{
    /// <summary>
    /// The reply data.
    /// </summary>
    public class ReplyData
    {
	    /// <summary>
	    /// Gets or sets the provider transaction id.
	    /// </summary>
	    public string ProviderTransactionId { get; set; }

		/// <summary>
		/// Gets or sets the address info code.
		/// </summary>
		public string AddressInfoCode { get; set; }

        /// <summary>
        /// Gets or sets the factor code.
        /// </summary>
        public string FactorCode { get; set; }

        /// <summary>
        /// Gets or sets the score.
        /// </summary>
        public int? Score { get; set; }

        /// <summary>
        /// Gets or sets the bin country.
        /// </summary>
        public string BinCountry { get; set; }

        /// <summary>
        /// Gets or sets the card issuer.
        /// </summary>
        public string CardIssuer { get; set; }

        /// <summary>
        /// Gets or sets the card scheme.
        /// </summary>
        public string CardScheme { get; set; }

        /// <summary>
        /// Gets or sets the host severity.
        /// </summary>
        public int HostSeverity { get; set; }

        /// <summary>
        /// Gets or sets the hot list info code.
        /// </summary>
        public string HotListInfoCode { get; set; }

        /// <summary>
        /// Gets or sets the identity info code.
        /// </summary>
        public string IdentityInfoCode { get; set; }

        /// <summary>
        /// Gets or sets the internet info code.
        /// </summary>
        public string InternetInfoCode { get; set; }

        /// <summary>
        /// Gets or sets the ip city.
        /// </summary>
        public string IpCity { get; set; }

        /// <summary>
        /// Gets or sets the ip country.
        /// </summary>
        public string IpCountry { get; set; }

        /// <summary>
        /// Gets or sets the ip routing method.
        /// </summary>
        public string IpRoutingMethod { get; set; }

        /// <summary>
        /// Gets or sets the ip state.
        /// </summary>
        public string IpState { get; set; }

        /// <summary>
        /// Gets or sets the phone info code.
        /// </summary>
        public string PhoneInfoCode { get; set; }

        /// <summary>
        /// Gets or sets the score model used.
        /// </summary>
        public string ScoreModelUsed { get; set; }

        /// <summary>
        /// Gets or sets the velocity info code.
        /// </summary>
        public string VelocityInfoCode { get; set; }

        /// <summary>
        /// Gets or sets the case priority.
        /// </summary>
        public int? CasePriority { get; set; }

        /// <summary>
        /// Gets or sets the finger print.
        /// </summary>
        public FingerPrintData FingerPrint { get; set; }
    }
}