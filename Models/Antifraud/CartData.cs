﻿namespace PaymentCwi.Models.Antifraud
{
    using System.Collections.Generic;

	/// <summary>
	/// The cart data.
	/// </summary>
	public class CartData
    {
        /// <summary>
        /// Gets or sets a value indicating whether is gift.
        /// </summary>
        public bool? IsGift { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether returns accepted.
        /// </summary>
        public bool? ReturnsAccepted { get; set; }

        /// <summary>
        /// Gets or sets the items.
        /// </summary>
        public List<ItemData> Items { get; set; }
    }
}