﻿namespace PaymentCwi.Models.Antifraud
{
    /// <summary>
    /// The merchant defined field.
    /// </summary>
    public class MerchantDefinedField
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public byte Id { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        public string Value { get; set; }
    }
}