﻿namespace PaymentCwi.Models.Antifraud
{
    using System;
    using System.Collections.Generic;
    using PaymentCwi.Models.Enum;

    /// <summary>
    /// The fraud analysis.
    /// </summary>
    public class FraudAnalysis
    {
	    /// <summary>
	    /// Gets or sets the provider transaction id.
	    /// </summary>
	    public string ProviderTransactionId { get; set; }

		/// <summary>
		/// Gets or sets the sequence.
		/// </summary>
		public AntifraudSequenceEnum? Sequence { get; set; }

        /// <summary>
        /// Gets or sets the sequence criteria.
        /// </summary>
        public AntifraudSequenceCriteriaEnum? SequenceCriteria { get; set; }

        /// <summary>
        /// Gets or sets the finger print id.
        /// </summary>
        public string FingerPrintId { get; set; }
		
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public Guid? Id { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        public FraudAnalysisStatusEnum? Status { get; set; }

		/// <summary>
		/// Gets or sets the status description
		/// </summary>
	    public string StatusDescription { get; set; }

		/// <summary>
		/// Gets or sets the previous status.
		/// </summary>
		public FraudAnalysisStatusEnum? PreviousStatus { get; set; }

        /// <summary>
        /// Gets or sets the provider.
        /// </summary>
        public string Provider { get; set; }

        /// <summary>
        /// Gets or sets the fraud analysis reason code.
        /// </summary>
        public int? FraudAnalysisReasonCode { get; set; }

	    /// <summary>
	    /// Gets or sets the score
	    /// </summary>
	    public int? Score { get; set; }

		/// <summary>
		/// Gets or sets the invalid fields.
		/// </summary>
		public List<string> InvalidFields { get; set; }

        /// <summary>
        /// Gets or sets the capture on low risk.
        /// </summary>
        public bool? CaptureOnLowRisk { get; set; }

        /// <summary>
        /// Gets or sets the void on high risk.
        /// </summary>
        public bool? VoidOnHighRisk { get; set; }

	    /// <summary>
	    /// Gets or sets the total order amount.
	    /// </summary>
	    public long? TotalOrderAmount { get; set; }

	    /// <summary>
	    /// Gets or sets the order date.
	    /// </summary>
	    public DateTime? OrderDate { get; set; }

	    /// <summary>
	    /// Gets or sets the is retry transaction.
	    /// </summary>
	    public bool? IsRetryTransaction { get; set; }

	    /// <summary>
	    /// Gets or sets the spliting payment method.
	    /// </summary>
	    public string SplitingPaymentMethod { get; set; }

	    /// <summary>
	    /// Gets or sets the merchant defined fields.
	    /// </summary>
	    public List<MerchantDefinedField> MerchantDefinedFields { get; set; }

	    /// <summary>
	    /// Gets or sets the cart.
	    /// </summary>
	    public CartData Cart { get; set; }

	    /// <summary>
	    /// Gets or sets the travel.
	    /// </summary>
	    public TravelData Travel { get; set; }

	    /// <summary>
	    /// Gets or sets the browser.
	    /// </summary>
	    public BrowserData Browser { get; set; }

	    /// <summary>
	    /// Gets or sets the shipping.
	    /// </summary>
	    public ShippingData Shipping { get; set; }

	    /// <summary>
	    /// Gets or sets the reply data.
	    /// </summary>
	    public ReplyData ReplyData { get; set; }
	}
}