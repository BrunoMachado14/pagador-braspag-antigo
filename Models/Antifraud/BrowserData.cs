﻿namespace PaymentCwi.Models.Antifraud
{
    /// <summary>
    /// The browser data.
    /// </summary>
    public class BrowserData
    {
        /// <summary>
        /// Gets or sets the host name.
        /// </summary>
        public string HostName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether cookies accepted.
        /// </summary>
        public bool CookiesAccepted { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the ip address.
        /// </summary>
        public string IpAddress { get; set; }
    }
}