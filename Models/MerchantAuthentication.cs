﻿namespace PaymentCwi.Models
{
    using System;

    /// <summary>
    /// The merchant authentication of Payment.
    /// </summary>
    public class MerchantAuthentication
    {
        /// <summary>
        /// Gets or sets the merchant id.
        /// </summary>
        public Guid MerchantId { get; set; }

        /// <summary>
        /// Gets or sets the merchant key.
        /// </summary>
        public string MerchantKey { get; set; }
    }
}