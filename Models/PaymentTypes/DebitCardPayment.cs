﻿namespace PaymentCwi.Models.PaymentTypes
{
    using PaymentCwi.Models;

    /// <summary>
    /// The debit card payment.
    /// </summary>
    public class DebitCardPayment : Payment
	{
	    /// <summary>
	    /// Initializes a new instance of the <see cref="DebitCardPayment"/> class.
	    /// </summary>
	    public DebitCardPayment()
		{
			this.Type = "DebitCard";
		}

	    /// <summary>
	    /// Gets or sets the debit card.
	    /// </summary>
	    public Card DebitCard { get; set; }

	    /// <summary>
	    /// Gets or sets the authentication url.
	    /// </summary>
	    public string AuthenticationUrl { get; set; }

	    /// <summary>
	    /// Gets or sets the proof of sale.
	    /// </summary>
	    public string ProofOfSale { get; set; }

	    /// <summary>
	    /// Gets or sets the acquirer transaction id.
	    /// </summary>
	    public string AcquirerTransactionId { get; set; }

	    /// <summary>
	    /// Gets or sets the authorization code.
	    /// </summary>
	    public string AuthorizationCode { get; set; }

	    /// <summary>
	    /// Gets or sets the soft descriptor.
	    /// </summary>
	    public string SoftDescriptor { get; set; }

	    /// <summary>
	    /// Gets or sets the eci.
	    /// </summary>
	    public string Eci { get; set; }
	}
}