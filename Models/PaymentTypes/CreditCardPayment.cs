﻿namespace PaymentCwi.Models.PaymentTypes
{
    using PaymentCwi.Models.Antifraud;
    using PaymentCwi.Models.Enum;

    /// <summary>
    /// The credit card payment.
    /// </summary>
    public class CreditCardPayment : Payment
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CreditCardPayment"/> class.
        /// </summary>
        public CreditCardPayment()
        {
            Type = "CreditCard";
        }

        /// <summary>
        /// Gets or sets the service tax amount.
        /// </summary>
        public long? ServiceTaxAmount { get; set; }

        /// <summary>
        /// Gets or sets the installments.
        /// </summary>
        public short? Installments { get; set; }

        /// <summary>
        /// Gets or sets the interest.
        /// </summary>
        public InterestTypeEnum? Interest { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether capture.
        /// </summary>
        public bool? Capture { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether authenticate.
        /// </summary>
        public bool? Authenticate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether recurrent.
        /// </summary>
        public bool? Recurrent { get; set; }

        /// <summary>
        /// Gets or sets the credit card.
        /// </summary>
        public Card CreditCard { get; set; }

	    /// <summary>
	    /// Gets or sets the new card.
	    /// </summary>
	    public Card NewCard { get; set; }

	    /// <summary>
	    /// Gets or sets the avs.
	    /// </summary>
	    public Avs Avs { get; set; }

		/// <summary>
		/// Gets or sets the authentication url.
		/// </summary>
		public string AuthenticationUrl { get; set; }

        /// <summary>
        /// Gets or sets the proof of sale.
        /// </summary>
        public string ProofOfSale { get; set; }

        /// <summary>
        /// Gets or sets the acquirer transaction id.
        /// </summary>
        public string AcquirerTransactionId { get; set; }

        /// <summary>
        /// Gets or sets the authorization code.
        /// </summary>
        public string AuthorizationCode { get; set; }

        /// <summary>
        /// Gets or sets the soft descriptor.
        /// </summary>
        public string SoftDescriptor { get; set; }

        /// <summary>
        /// Gets or sets the fraud analysis.
        /// </summary>
        public FraudAnalysis FraudAnalysis { get; set; }

        /// <summary>
        /// Gets or sets the external authentication.
        /// </summary>
        public ExternalAuthentication ExternalAuthentication { get; set; }
	}
}