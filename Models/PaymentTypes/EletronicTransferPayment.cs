﻿namespace PaymentCwi.Models.PaymentTypes
{
    /// <summary>
    /// The eletronic transfer payment.
    /// </summary>
    public class EletronicTransferPayment : Payment
	{
	    /// <summary>
	    /// Initializes a new instance of the <see cref="EletronicTransferPayment"/> class.
	    /// </summary>
	    public EletronicTransferPayment()
        {
            Type = "EletronicTransfer";
        }

	    /// <summary>
	    /// Gets or sets the url.
	    /// </summary>
	    public string Url { get; set; }

	}
}