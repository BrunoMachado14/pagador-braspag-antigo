﻿namespace PaymentCwi.Models.PaymentTypes
{
    /// <summary>
    /// The boleto payment.
    /// </summary>
    public class BoletoPayment : Payment
	{
		public BoletoPayment()
		{
			Type = "Boleto";
		}

	    /// <summary>
	    /// Gets or sets the instructions.
	    /// </summary>
	    public string Instructions { get; set; }

	    /// <summary>
	    /// Gets or sets the expiration date.
	    /// </summary>
	    public string ExpirationDate { get; set; }

	    /// <summary>
	    /// Gets or sets the demostrative.
	    /// </summary>
	    public string Demostrative { get; set; }

	    /// <summary>
	    /// Gets or sets the url.
	    /// </summary>
	    public string Url { get; set; }

	    /// <summary>
	    /// Gets or sets the boleto number.
	    /// </summary>
	    public string BoletoNumber { get; set; }

	    /// <summary>
	    /// Gets or sets the bar code number.
	    /// </summary>
	    public string BarCodeNumber { get; set; }

	    /// <summary>
	    /// Gets or sets the digitable line.
	    /// </summary>
	    public string DigitableLine { get; set; }

	    /// <summary>
	    /// Gets or sets the assignor.
	    /// </summary>
	    public string Assignor { get; set; }

	    /// <summary>
	    /// Gets or sets the address.
	    /// </summary>
	    public string Address { get; set; }

	    /// <summary>
	    /// Gets or sets the identification.
	    /// </summary>
	    public string Identification { get; set; }

	    /// <summary>
	    /// Gets or sets a value indicating whether is recurring.
	    /// </summary>
	    public bool IsRecurring { get; set; }
    }
}