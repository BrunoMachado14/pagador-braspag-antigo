﻿namespace PaymentCwi.Models
{
    /// <summary>
    /// The avs.
    /// </summary>
    public class Avs
    {
        /// <summary>
        /// Gets or sets the cpf.
        /// </summary>
        public string Cpf { get; set; }

        /// <summary>
        /// Gets or sets the zip code.
        /// </summary>
        public string ZipCode { get; set; }

        /// <summary>
        /// Gets or sets the street.
        /// </summary>
        public string Street { get; set; }

        /// <summary>
        /// Gets or sets the number.
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// Gets or sets the complement.
        /// </summary>
        public string Complement { get; set; }

        /// <summary>
        /// Gets or sets the district.
        /// </summary>
        public string District { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        public int? Status { get; set; }

        /// <summary>
        /// Gets or sets the return code.
        /// </summary>
        public string ReturnCode { get; set; }
    }
}