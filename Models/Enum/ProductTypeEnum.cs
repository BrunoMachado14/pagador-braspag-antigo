﻿namespace PaymentCwi.Models.Enum
{
    /// <summary>
    /// The product type enum.
    /// </summary>
    public enum ProductTypeEnum
    {
        /// <summary>
        /// The undefined.
        /// </summary>
        Undefined = 0,

        /// <summary>
        /// The adult content.
        /// </summary>
        AdultContent = 1,

        /// <summary>
        /// The coupon.
        /// </summary>
        Coupon = 2,

        /// <summary>
        /// The default.
        /// </summary>
        Default = 3,

        /// <summary>
        /// The eletronic good.
        /// </summary>
        EletronicGood = 4,

        /// <summary>
        /// The eletronic software.
        /// </summary>
        EletronicSoftware = 5,

        /// <summary>
        /// The gift certificate.
        /// </summary>
        GiftCertificate = 6,

        /// <summary>
        /// The handling only.
        /// </summary>
        HandlingOnly = 7,

        /// <summary>
        /// The service.
        /// </summary>
        Service = 8,

        /// <summary>
        /// The shipping and handling.
        /// </summary>
        ShippingAndHandling = 9,

        /// <summary>
        /// The shipping only.
        /// </summary>
        ShippingOnly = 10,

        /// <summary>
        /// The subscription.
        /// </summary>
        Subscription = 11,
    }
}