﻿namespace PaymentCwi.Models.Enum
{
    /// <summary>
    /// The provider enum.
    /// </summary>
    public enum ProviderEnum
    {
        /// <summary>
        /// The cielo.
        /// </summary>
        Cielo,

        /// <summary>
        /// The redecard.
        /// </summary>
        Redecard,

        /// <summary>
        /// The simulado.
        /// </summary>
        Simulado,

        /// <summary>
        /// The bradesco.
        /// </summary>
        Bradesco,

        /// <summary>
        /// The itau.
        /// </summary>
        Itau,

        /// <summary>
        /// The brb.
        /// </summary>
        Brb,

        /// <summary>
        /// The caixa.
        /// </summary>
        Caixa,

        /// <summary>
        /// The banco do brasil.
        /// </summary>
        BancoDoBrasil,

        /// <summary>
        /// The citibank.
        /// </summary>
        Citibank,

        /// <summary>
        /// The santander.
        /// </summary>
        Santander,

        /// <summary>
        /// The hsbc.
        /// </summary>
        Hsbc,

        /// <summary>
        /// The safety pay.
        /// </summary>
        SafetyPay,

        /// <summary>
        /// The cielo si tef.
        /// </summary>
        CieloSiTef,

        /// <summary>
        /// The rede si tef.
        /// </summary>
        RedeSiTef,

        /// <summary>
        /// The santander si tef.
        /// </summary>
        SantanderSiTef,

        /// <summary>
        /// The rede.
        /// </summary>
        Rede,

        /// <summary>
        /// The banorte.
        /// </summary>
        Banorte,

        /// <summary>
        /// The sub 1.
        /// </summary>
        Sub1,

        /// <summary>
        /// The banco do brasil 2.
        /// </summary>
        BancoDoBrasil2,

        /// <summary>
        /// The cielo 30.
        /// </summary>
        Cielo30,

        /// <summary>
        /// The bradesco 2.
        /// </summary>
        Bradesco2,

        /// <summary>
        /// The transbank.
        /// </summary>
        Transbank,

        /// <summary>
        /// The riachuelo si tef.
        /// </summary>
        RiachueloSiTef,

        /// <summary>
        /// The dm card.
        /// </summary>
        DmCard,

        /// <summary>
        /// The citibank 2.
        /// </summary>
        Citibank2,

        /// <summary>
        /// The itau shopline.
        /// </summary>
        ItauShopline,

        /// <summary>
        /// The banese.
        /// </summary>
        Banese,

        /// <summary>
        /// The global payments.
        /// </summary>
        GlobalPayments,

        /// <summary>
        /// The first data.
        /// </summary>
        FirstData,

        /// <summary>
        /// The getnet.
        /// </summary>
        Getnet
    }
}