﻿namespace PaymentCwi.Models.Enum
{
	/// <summary>
	/// The change type notification enum.
	/// </summary>
	public enum ChangeTypeNotificationEnum
	{
		/// <summary>
		/// The status changed.
		/// </summary>
		StatusChanged = 1,

		/// <summary>
		/// The recurrence created.
		/// </summary>
		RecurrenceCreated = 2,

		/// <summary>
		/// The anti fraud status changed.
		/// </summary>
		AntiFraudStatusChanged = 3,

		/// <summary>
		/// The recurring payment status changed.
		/// </summary>
		RecurringPaymentStatusChanged = 4,

		/// <summary>
		/// The reversal denied.
		/// </summary>
		ReversalDenied = 5,

		/// <summary>
		/// The registered ticket paid.
		/// </summary>
		RegisteredTicketPaid = 6
	}
}