﻿namespace PaymentCwi.Models.Enum
{
    /// <summary>
    /// The transaction status.
    /// </summary>
    public enum TransactionStatus : byte
    {
        /// <summary>
        /// The not finished.
        /// </summary>
        NotFinished = 0,

        /// <summary>
        /// The authorized.
        /// </summary>
        Authorized = 1,

        /// <summary>
        /// The payment confirmed.
        /// </summary>
        PaymentConfirmed = 2,

        /// <summary>
        /// The denied.
        /// </summary>
        Denied = 3,

        /// <summary>
        /// The voided.
        /// </summary>
        Voided = 10,

        /// <summary>
        /// The refunded.
        /// </summary>
        Refunded = 11,

        /// <summary>
        /// The pending.
        /// </summary>
        Pending = 12,

        /// <summary>
        /// The aborted.
        /// </summary>
        Aborted = 13,

        /// <summary>
        /// The scheduled.
        /// </summary>
        Scheduled = 20
    }
}