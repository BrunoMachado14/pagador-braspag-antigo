﻿namespace PaymentCwi.Models.Enum
{
    /// <summary>
    /// The gift category enum.
    /// </summary>
    public enum GiftCategoryEnum
    {
        /// <summary>
        /// The undefined.
        /// </summary>
        Undefined = 0,

        /// <summary>
        /// The yes.
        /// </summary>
        Yes = 1,

        /// <summary>
        /// The no.
        /// </summary>
        No = 2,

        /// <summary>
        /// The off.
        /// </summary>
        Off = 3,
    }
}