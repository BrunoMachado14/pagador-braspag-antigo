﻿namespace PaymentCwi.Models.Enum
{
	/// <summary>
	/// The customer status enum.
	/// </summary>
	public enum CustomerStatusEnum
	{
		/// <summary>
		/// The new.
		/// </summary>
		New,

		/// <summary>
		/// The existing.
		/// </summary>
		Existing
    }
}