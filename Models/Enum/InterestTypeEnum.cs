﻿namespace PaymentCwi.Models.Enum
{
    /// <summary>
    /// The interest type enum.
    /// </summary>
    public enum InterestTypeEnum
    {
        /// <summary>
        /// The by merchant.
        /// </summary>
        ByMerchant,

        /// <summary>
        /// The by issuer.
        /// </summary>
        ByIssuer
    }
}