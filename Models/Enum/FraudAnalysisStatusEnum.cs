﻿namespace PaymentCwi.Models.Enum
{
	/// <summary>
	/// The fraud analysis status enum.
	/// </summary>
	public enum FraudAnalysisStatusEnum
	{
		/// <summary>
		/// The unknown.
		/// </summary>
		Unknown = 0,

		/// <summary>
		/// The accept.
		/// </summary>
		Accept = 1,

		/// <summary>
		/// The reject.
		/// </summary>
		Reject = 2,

		/// <summary>
		/// The review.
		/// </summary>
		Review = 3,

		/// <summary>
		/// The aborted.
		/// </summary>
		Aborted = 4,

		/// <summary>
		/// The error.
		/// </summary>
		Error = 5
    }
}