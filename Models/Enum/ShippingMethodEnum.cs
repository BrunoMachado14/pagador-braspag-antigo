﻿namespace PaymentCwi.Models.Enum
{
    /// <summary>
    /// The shipping method enum.
    /// </summary>
    public enum ShippingMethodEnum
    {
        /// <summary>
        /// The undefined.
        /// </summary>
        Undefined = 0,

        /// <summary>
        /// The same day.
        /// </summary>
        SameDay = 1,

        /// <summary>
        /// The one day.
        /// </summary>
        OneDay = 2,

        /// <summary>
        /// The two day.
        /// </summary>
        TwoDay = 3,

        /// <summary>
        /// The three day.
        /// </summary>
        ThreeDay = 4,

        /// <summary>
        /// The low cost.
        /// </summary>
        LowCost = 5,

        /// <summary>
        /// The pickup.
        /// </summary>
        Pickup = 6,

        /// <summary>
        /// The other.
        /// </summary>
        Other = 7,

        /// <summary>
        /// The none.
        /// </summary>
        None = 8,
    }
}