﻿namespace PaymentCwi.Models.Enum
{
    /// <summary>
    /// The antifraud sequence enum.
    /// </summary>
    public enum AntifraudSequenceEnum
    {
        /// <summary>
        /// The analyse first.
        /// </summary>
        AnalyseFirst = 0,

        /// <summary>
        /// The authorize first.
        /// </summary>
        AuthorizeFirst = 1,
    }
}