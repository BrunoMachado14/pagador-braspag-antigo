﻿namespace PaymentCwi.Models.Enum
{
    /// <summary>
    /// The passenger rating enum.
    /// </summary>
    public enum PassengerRatingEnum
    {
        /// <summary>
        /// The undefined.
        /// </summary>
        Undefined = 0,

        /// <summary>
        /// The adult.
        /// </summary>
        Adult = 1,

        /// <summary>
        /// The child.
        /// </summary>
        Child = 2,

        /// <summary>
        /// The infant.
        /// </summary>
        Infant = 3,

        /// <summary>
        /// The youth.
        /// </summary>
        Youth = 4,

        /// <summary>
        /// The student.
        /// </summary>
        Student = 5,

        /// <summary>
        /// The senior citizen.
        /// </summary>
        SeniorCitizen = 6,

        /// <summary>
        /// The military.
        /// </summary>
        Military = 7,
    }
}