﻿namespace PaymentCwi.Models.Enum
{
    /// <summary>
    /// The hedge enum.
    /// </summary>
    public enum HedgeEnum
    {
        /// <summary>
        /// The undefined.
        /// </summary>
        Undefined = 0,

        /// <summary>
        /// The low.
        /// </summary>
        Low = 1,

        /// <summary>
        /// The normal.
        /// </summary>
        Normal = 2,

        /// <summary>
        /// The high.
        /// </summary>
        High = 3,

        /// <summary>
        /// The off.
        /// </summary>
        Off = 4,
    }
}