﻿namespace PaymentCwi.Models
{
    /// <summary>
    /// The sale.
    /// </summary>
    public class Sale : BaseResponse
    {
        /// <summary>
        /// Gets or sets the merchant order id.
        /// </summary>
        public string MerchantOrderId { get; set; }

        /// <summary>
        /// Gets or sets the customer.
        /// </summary>
        public Customer Customer { get; set; }

        /// <summary>
        /// Gets or sets the payment.
        /// </summary>
        public Payment Payment { get; set; }
    }
}