﻿namespace PaymentCwi.Models
{
	/// <summary>
	/// The payment type enum.
	/// </summary>
	public enum PaymentTypeEnum
	{
		/// <summary>
		/// The credit card.
		/// </summary>
		CreditCard,

		/// <summary>
		/// The debit card.
		/// </summary>
		DebitCard,

		/// <summary>
		/// The boleto.
		/// </summary>
		Boleto,

		/// <summary>
		/// The eletronic transfer.
		/// </summary>
		EletronicTransfer
	}
}