﻿namespace PaymentCwi.Models
{
    using System;

    using PaymentCwi.Models.Enum;

	/// <summary>
    /// The customer.
    /// </summary>
    public class Customer
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the identity.
        /// </summary>
        public string Identity { get; set; }

        /// <summary>
        /// Gets or sets the identity type.
        /// </summary>
        public string IdentityType { get; set; }

	    /// <summary>
	    /// Gets or sets the work phone.
	    /// </summary>
	    public string WorkPhone { get; set; }

	    /// <summary>
	    /// Gets or sets the mobile.
	    /// </summary>
	    public string Mobile { get; set; }

	    /// <summary>
	    /// Gets or sets the phone.
	    /// </summary>
	    public string Phone { get; set; }

		/// <summary>
		/// Gets or sets the email.
		/// </summary>
		public string Email { get; set; }

        /// <summary>
        /// Gets or sets the birthdate.
        /// </summary>
        public DateTime? Birthdate { get; set; }

        /// <summary>
        /// Gets or sets the address.
        /// </summary>
        public Address Address { get; set; }

        /// <summary>
        /// Gets or sets the delivery address.
        /// </summary>
        public Address DeliveryAddress { get; set; }

	    /// <summary>
	    /// Gets or sets the status.
	    /// </summary>
	    public CustomerStatusEnum? Status { get; set; }
	}
}