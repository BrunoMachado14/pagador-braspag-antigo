﻿namespace PaymentCwi.Models
{
    using System;
    using PaymentCwi.Models.Enum;

    /// <summary>
    /// The recurrent payment.
    /// </summary>
    public class RecurrentPayment
    {
        /// <summary>
        /// Gets or sets the recurrent payment id.
        /// </summary>
        public Guid? RecurrentPaymentId { get; set; }

        /// <summary>
        /// Gets or sets the reason code.
        /// </summary>
        public int ReasonCode { get; set; }

        /// <summary>
        /// Gets or sets the reason message.
        /// </summary>
        public string ReasonMessage { get; set; }

        /// <summary>
        /// Gets or sets the next recurrency.
        /// </summary>
        public DateTime? NextRecurrency { get; set; }

        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Gets or sets the interval.
        /// </summary>
        public RecurrencyIntervalEnum Interval { get; set; }

        /// <summary>
        /// Gets or sets the link.
        /// </summary>
        public Link Link { get; set; }

        /// <summary>
        /// Gets or sets the authorize now.
        /// </summary>
        public bool? AuthorizeNow { get; set; }
    }
}