﻿namespace PaymentCwi.Models
{
    using System;
    using System.Collections.Generic;
    using Newtonsoft.Json;
    using PaymentCwi.Models.Enum;
    using PaymentCwi.Services;

    /// <summary>
    /// The payment.
    /// </summary>
    [JsonConverter(typeof(PaymentConverter))]
    public class Payment
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Payment"/> class.
        /// </summary>
        public Payment()
        {
            Country = "BRA";
            Currency = "BRL";
        }

        /// <summary>
        /// Gets or sets the payment id.
        /// </summary>
        public Guid? PaymentId { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the amount.
        /// </summary>
        public long? Amount { get; set; }

        /// <summary>
        /// Gets or sets the received date.
        /// </summary>
        public DateTime? ReceivedDate { get; set; }

        /// <summary>
        /// Gets or sets the captured amount.
        /// </summary>
        public long? CapturedAmount { get; set; }

        /// <summary>
        /// Gets or sets the voided amount.
        /// </summary>
        public long? VoidedAmount { get; set; }

        /// <summary>
        /// Gets or sets the currency.
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Gets or sets the provider.1
        /// </summary>
        public ProviderEnum? Provider { get; set; }

        /// <summary>
        /// Gets or sets the credentials.
        /// </summary>
        public PaymentCredentials Credentials { get; set; }

        /// <summary>
        /// Gets or sets the return url.
        /// </summary>
        public string ReturnUrl { get; set; }

        /// <summary>
        /// Gets or sets the extra data collection.
        /// </summary>
        public List<ExtraData> ExtraDataCollection { get; set; }

        /// <summary>
        /// Gets or sets the reason code.
        /// </summary>
        public byte ReasonCode { get; set; }

        /// <summary>
        /// Gets or sets the reason message.
        /// </summary>
        public string ReasonMessage { get; set; }

        /// <summary>
        /// Gets or sets the provider return code.
        /// </summary>
        public string ProviderReturnCode { get; set; }

        /// <summary>
        /// Gets or sets the provider return message.
        /// </summary>
        public string ProviderReturnMessage { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        public TransactionStatus? Status { get; set; }

        /// <summary>
        /// Gets or sets the recurrent payment.
        /// </summary>
        public RecurrentPayment RecurrentPayment { get; set; }

        /// <summary>
        /// Gets or sets the links.
        /// </summary>
        public List<Link> Links { get; set; }
    }
}