﻿namespace PaymentCwi.Services
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using Newtonsoft.Json;
    using PaymentCwi.Models;
    using RestSharp;
    using RestSharp.Deserializers;

    public class PaymentApiService : IPaymentApiService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PaymentApiService"/> class.
        /// </summary>
        /// <param name="url">
        /// The url.
        /// </param>
        public PaymentApiService(string url)
        {
            RestClient = new RestClient { BaseUrl = new Uri(url) };
            JsonDeserializer = new JsonDeserializer();
        }

        /// <summary>
        /// Gets or sets the rest client.
        /// </summary>
        public IRestClient RestClient { get; set; }

        /// <summary>
        /// Gets or sets the json deserializer.
        /// </summary>
        protected IDeserializer JsonDeserializer { get; set; }

	    /// <summary>The create sale.</summary>
	    /// <param name="sale">The sale.</param>
	    /// <param name="merchantAuthentication">The merchant authentication.</param>
	    /// <param name="headers">The headers.</param>
	    /// <returns>The <see cref="Sale"/>.</returns>
	    public Sale CreateSale(Sale sale, MerchantAuthentication merchantAuthentication, Dictionary<string, string> headers = null)
        {
            var restRequest = new RestRequest(@"sales", Method.POST) { RequestFormat = DataFormat.Json };
            AddHeaders(restRequest, merchantAuthentication);
            if (headers != null)
                AddCustomHeaders(restRequest, headers);

            return PostSale(sale, restRequest);
        }

        /// <summary>The capture.</summary>
        /// <param name="paymentId">The payment id.</param>
        /// <param name="merchantAuthentication">The merchant authentication.</param>
        /// <param name="captureRequest">The capture request.</param>
        /// <returns>The Capture Response.</returns>
        public CaptureResponse Capture(Guid paymentId, MerchantAuthentication merchantAuthentication, CaptureRequest captureRequest = null)
        {
            var restRequest = new RestRequest(@"sales/{paymentId}/capture", Method.PUT) { RequestFormat = DataFormat.Json };
            AddHeaders(restRequest, merchantAuthentication);

            restRequest.AddUrlSegment("paymentId", paymentId.ToString());

            if (captureRequest != null)
            {
                restRequest.AddQueryParameter("amount", captureRequest.Amount.ToString());
                restRequest.AddQueryParameter("serviceTaxAmount", captureRequest.ServiceTaxAmount.ToString());
            }

            var response = RestClient.Execute<CaptureResponse>(restRequest);

            CaptureResponse captureResponse = null;

            if (response.StatusCode == HttpStatusCode.OK)
                captureResponse = JsonConvert.DeserializeObject<CaptureResponse>(response.Content);
            else if (response.StatusCode == HttpStatusCode.BadRequest)
                captureResponse = new CaptureResponse { ErrorDataCollection = JsonDeserializer.Deserialize<List<Error>>(response) };
            else
                captureResponse = new CaptureResponse();

            captureResponse.HttpStatus = response.StatusCode;

            return captureResponse;
        }

        /// <summary>The void.</summary>
        /// <param name="paymentId">The payment id.</param>
        /// <param name="merchantAuthentication">The merchant authentication.</param>
        /// <param name="voidRequest">The void request.</param>
        /// <returns>The Void Response.</returns>
        public VoidResponse Void(Guid paymentId, MerchantAuthentication merchantAuthentication, VoidRequest voidRequest = null)
        {
            var restRequest = new RestRequest(@"sales/{paymentId}/void", Method.PUT) { RequestFormat = DataFormat.Json };
            AddHeaders(restRequest, merchantAuthentication);

            restRequest.AddUrlSegment("paymentId", paymentId.ToString());

            if (voidRequest.Amount != null)
            {
                restRequest.AddQueryParameter("amount", voidRequest.Amount.ToString());
            }

            var response = RestClient.Execute<VoidResponse>(restRequest);

            VoidResponse voidResponse = null;

            if (response.StatusCode == HttpStatusCode.OK)
                voidResponse = JsonConvert.DeserializeObject<VoidResponse>(response.Content);
            else if (response.StatusCode == HttpStatusCode.BadRequest)
                voidResponse = new VoidResponse { ErrorDataCollection = JsonDeserializer.Deserialize<List<Error>>(response) };
            else
                voidResponse = new VoidResponse();

            voidResponse.HttpStatus = response.StatusCode;

            return voidResponse;
        }

        /// <summary>The get of transaction by PaymentId.</summary>
        /// <param name="paymentId">The payment id.</param>
        /// <param name="merchantAuthentication">The merchant authentication.</param>
        /// <returns>The Sale found</returns>
        public Sale Get(Guid paymentId, MerchantAuthentication merchantAuthentication)
        {
            var restRequest = new RestRequest(@"sales/{paymentId}", Method.GET) { RequestFormat = DataFormat.Json };
            AddHeaders(restRequest, merchantAuthentication);

            restRequest.AddUrlSegment("paymentId", paymentId.ToString());

            var response = RestClient.Execute<Sale>(restRequest);

            Sale saleResponse = null;

            if (response.StatusCode == HttpStatusCode.OK)
                saleResponse = JsonConvert.DeserializeObject<Sale>(response.Content);
            else if (response.StatusCode == HttpStatusCode.BadRequest)
                saleResponse = new Sale { ErrorDataCollection = JsonDeserializer.Deserialize<List<Error>>(response) };
            else
                saleResponse = new Sale();

            saleResponse.HttpStatus = response.StatusCode;

            return saleResponse;
        }

        /// <summary>The add headers.</summary>
        /// <param name="request">The request.</param>
        /// <param name="auth">The auth.</param>
        private void AddHeaders(IRestRequest request, MerchantAuthentication auth)
        {
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("MerchantId", auth.MerchantId.ToString());
            request.AddHeader("MerchantKey", auth.MerchantKey);
        }

        /// <summary>The add custom headers.</summary>
        /// <param name="request">The request.</param>
        /// <param name="customHeaders">The custom headers.</param>
        private void AddCustomHeaders(IRestRequest request, Dictionary<string, string> customHeaders)
        {
            foreach (var header in customHeaders)
            {
                request.AddHeader(header.Key, header.Value);
            }
        }

        /// <summary>The post sale.</summary>
        /// <param name="sale">The sale for post.</param>
        /// <param name="restRequest">The rest request.</param>
        /// <returns>The sale posted.</returns>
        private Sale PostSale(Sale sale, RestRequest restRequest)
        {
			var settings = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore };
			var body = JsonConvert.SerializeObject(sale, Formatting.Indented, settings);
			
	        restRequest.AddParameter("application/json", body, ParameterType.RequestBody);
			
			var response = RestClient.Execute<Sale>(restRequest);
			
            Sale saleResponse = null;

            if (response.StatusCode == HttpStatusCode.Created)
                saleResponse = JsonConvert.DeserializeObject<Sale>(response.Content);
            else if (response.StatusCode == HttpStatusCode.BadRequest)
                saleResponse = new Sale { ErrorDataCollection = JsonDeserializer.Deserialize<List<Error>>(response) };
			else if (response.StatusCode == 0)
			{
				saleResponse = HandleErrorException(response);
			}
			else
                saleResponse = new Sale();

            saleResponse.HttpStatus = response.StatusCode;

            return saleResponse;
        }

        /// <summary>The handle error exception.</summary>
        /// <param name="response">The response.</param>
        /// <returns>The sale posted</returns>
        private Sale HandleErrorException(IRestResponse<Sale> response)
        {
            return new Sale
            {
                ErrorDataCollection = new List<Error>
                {
                    new Error
                        {
                            Code = -1,
                            Message = $"ErrorMessage: {response.ErrorMessage} | ErrorException: {response.ErrorException}"
                        }
                }
            };
        }
    }
}