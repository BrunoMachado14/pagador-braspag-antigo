﻿namespace PaymentCwi.Services
{
    using System;
    using System.Collections.Generic;

    using PaymentCwi.Models;

    public interface IPaymentApiService
    {
		/// <summary>The create sale.</summary>
		/// <param name="sale">The sale.</param>
		/// <param name="merchantAuthentication">The merchant authentication.</param>
		/// <param name="headers">The custom headers</param>
		/// <returns>The Sale posted</returns>
		Sale CreateSale(Sale sale, MerchantAuthentication merchantAuthentication, Dictionary<string, string> headers = null);

        /// <summary>The capture.</summary>
        /// <param name="paymentId">The payment id.</param>
        /// <param name="merchantAuthentication">The merchant authentication.</param>
        /// <param name="captureRequest">The capture request.</param>
        /// <returns>The Capture Response.</returns>
        CaptureResponse Capture(Guid paymentId, MerchantAuthentication merchantAuthentication, CaptureRequest captureRequest = null);

        /// <summary>The cancellation of payment by Payment Id</summary>
        /// <param name="paymentId">The payment id.</param>
        /// <param name="merchantAuthentication">The merchant authentication.</param>
        /// <param name="voidRequest">The void request.</param>
        /// <returns>The Void Response.</returns>
        VoidResponse Void(Guid paymentId, MerchantAuthentication merchantAuthentication, VoidRequest voidRequest = null);

        /// <summary>The get of transaction by PaymentId.</summary>
        /// <param name="paymentId">The payment id.</param>
        /// <param name="merchantAuthentication">The merchant authentication.</param>
        /// <returns>The Sale found</returns>
        Sale Get(Guid paymentId, MerchantAuthentication merchantAuthentication);
    }
}